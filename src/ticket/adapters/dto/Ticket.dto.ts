import { IsNotEmpty } from 'class-validator';

/**
 * TicketDto
 * Used for CUD ticket
 */
export class TicketDto {
  @IsNotEmpty()
  description: string;
  priority: number;
}
